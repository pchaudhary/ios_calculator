//
//  ViewController.swift
//  BestCalculator
//
//  Created by Cerebro on 23/04/15.
//  Copyright (c) 2015 CodeKamp. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var resultLabel: UILabel!
    var isEnteringNumber = false
    var firstNumber = 0.0
    var operation: String?
    
    @IBOutlet weak var buttonFour: UIButton!
    @IBOutlet weak var buttonOne: UIButton!
    @IBOutlet weak var buttonTwo: UIButton!
    @IBOutlet weak var buttonThree: UIButton!
    
    @IBOutlet weak var buttonFive: UIButton!
    
    @IBAction func digitButtonTapped(sender: UIButton) {
        
        if isEnteringNumber {
            resultLabel.text = resultLabel.text! + sender.currentTitle!
        } else {
            resultLabel.text = sender.currentTitle
            isEnteringNumber = true
        }
    }
    
    
    @IBAction func operationButtonPressed(sender: UIButton) {
        isEnteringNumber = false;
        calculate()
        
        operation = sender.currentTitle
        firstNumber = NSNumberFormatter().numberFromString(resultLabel.text!)!.doubleValue;
    }
    
    @IBAction func changeColor(sender: UIButton) {
        buttonOne.backgroundColor = UIColor.blueColor()
        buttonTwo.backgroundColor = UIColor.blueColor()
        buttonThree.backgroundColor = UIColor.blueColor()
        buttonFour.backgroundColor = UIColor.blueColor()
        buttonFive.backgroundColor = UIColor.blueColor()
        
        sender.backgroundColor = UIColor.redColor()
    }
    @IBAction func calculate() {
        
        if operation != nil {
            isEnteringNumber = false
            
            var result = 0.0
            var secondNumber = NSNumberFormatter().numberFromString(resultLabel.text!)!.doubleValue;
            
            if(operation == "+") {
                result = firstNumber + secondNumber
            } else if(operation == "-") {
                result = firstNumber - secondNumber
            } else if(operation == "/") {
                result = firstNumber / secondNumber
            } else if(operation == "*") {
                result = firstNumber * secondNumber
            }
            
            operation = nil
            resultLabel.text = "\(result)"
        }
        
    }
}

